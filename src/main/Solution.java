package main;

public class Solution {
    public static void main(String[] args) {
        QuadraticEquation equation = new QuadraticEquation(2, 4, 5);

        double[] x = equation.solution();

        for (double i: x) {
            System.out.println("найден корень: " + i);
        }
    }
}

