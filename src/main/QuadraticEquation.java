package main;

public class QuadraticEquation {
    private int a;
    private int b;
    private int c;

    public QuadraticEquation(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    private double discriminant() {
        double d;
            d = this.b*this.b - 4 * this.a * this.c;
        return d;
    }

    public double[] solution() {
        double[] x = {2.5, 3.5};
        //TODO написать решение уравнения при discriminant() >= 0
        return x;
    }


}
